﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Dal.DataAnnotations;
using Library.Enumerations;

namespace Library.ViewModels
{
	public class TESTViewModel
	{
		[FormProperty(ControlName = "txtInt1", ControlType = InputType.TextBox)]
		public int TxtInt1 { get; set; }

		[FormProperty(ControlName = "cmbxEnum1", ControlType = InputType.ComboBox)]
		public Enum1 Enum1 { get; set; }
		
		[FormProperty(ControlName = "listboxValue1", ControlType = InputType.ListBox)]
		public int Value { get; set; }

		[FormProperty(ControlName = "cmbxList2", ControlType = InputType.ComboBox)]
		public int ListValue { get; set; }

		[FormProperty(ControlName = "ListBoxList1", ControlType = InputType.ListBox)]
		public List<int> ListBoxList1 { get; set; }
	}

	public class TestClass
	{
		public int? TestProperty1 { get; set; }

		public string TestProperty2 { get; set; }
	}
}
