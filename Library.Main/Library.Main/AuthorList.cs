﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library.Services;
using Library.ViewModels;

namespace Library.Main
{
    public partial class AuthorList : Form
    {
        private AuthorService _authorService;
        private List<AuthorViewModel> _authors;
        public AuthorList()
        {
            InitializeComponent();
            _authorService = new AuthorService();
            ConfigureAuthorDataGrid();
            FillGrid();
        }

        private void FillGrid()
        {
            _authors = _authorService.GetAuthores();
            authorDataGridView.Rows.Clear();
            foreach (var author in _authors)
            {
                int index = authorDataGridView.Rows.Add();
                authorDataGridView.Rows[index].Cells["AuthorId"].Value = author.Id;
                authorDataGridView.Rows[index].Cells["Firstname"].Value = author.FirstName;
                authorDataGridView.Rows[index].Cells["Lastname"].Value = author.LastName;
                authorDataGridView.Rows[index].Cells["EditAthor"].Value = "Edit";
                authorDataGridView.Rows[index].Cells["AuthorObject"].Value = author;
                authorDataGridView.Rows[index].Cells["AuthorBooks"].Value = "Author Books";
            }
        }

        public void ConfigureAuthorDataGrid()
        {
            authorDataGridView.Columns.Add("AuthorId", "Author Id");
            authorDataGridView.Columns["AuthorId"].Visible = false;
            authorDataGridView.Columns["AuthorId"].Name = "AuthorId";
            authorDataGridView.Columns["AuthorId"].CellTemplate = new DataGridViewTextBoxCell();


            DataGridViewColumn authorFirstname = new DataGridViewColumn
            {
                Width = 120,
                HeaderText = "First Name",
                Resizable = DataGridViewTriState.False,
                AutoSizeMode = DataGridViewAutoSizeColumnMode.None,
                Name = "Firstname",
                DataPropertyName = "Firstname",
                CellTemplate = new DataGridViewTextBoxCell()
            };
            authorDataGridView.Columns.Add(authorFirstname);

            DataGridViewColumn authorLastName = new DataGridViewColumn
            {
                Width = 120,
                HeaderText = "Last Name",
                Resizable = DataGridViewTriState.False,
                AutoSizeMode = DataGridViewAutoSizeColumnMode.None,
                Name = "Lastname",
                DataPropertyName = "Lastname",
                CellTemplate = new DataGridViewTextBoxCell()
            };
            authorDataGridView.Columns.Add(authorLastName);

            DataGridViewButtonColumn editAuthorButton = new DataGridViewButtonColumn
            {
                Name = "EditAthor",
                Width = 70,
                HeaderText = "Edit",
                Text = "Edit",
                UseColumnTextForButtonValue = false,
                CellTemplate = new DataGridViewButtonCell()
            };
            authorDataGridView.Columns.Add(editAuthorButton);

            DataGridViewColumn authorObject = new DataGridViewColumn
            {
                Width = 120,
                HeaderText = "Last Name",
                Resizable = DataGridViewTriState.False,
                AutoSizeMode = DataGridViewAutoSizeColumnMode.None,
                Name = "AuthorObject",
                DataPropertyName = "AuthorObject",
                Visible = false,
                CellTemplate = new DataGridViewTextBoxCell()
            };
            authorDataGridView.Columns.Add(authorObject);

            DataGridViewButtonColumn authorBooksButton = new DataGridViewButtonColumn
            {
                Name = "AuthorBooks",
                Width = 110,
                HeaderText = "Author Books",
                Text = "Author Books",
                UseColumnTextForButtonValue = true,
                CellTemplate = new DataGridViewButtonCell()
            };
            authorDataGridView.Columns.Add(authorBooksButton);
        }

        private void authorDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                var author = (AuthorViewModel)((DataGridView)sender).Rows[e.RowIndex].Cells["AuthorObject"].Value;
                AuthorForm authorForm = new AuthorForm(author);
                authorForm.FormClosed += AuthorForm_FormClosed;
                authorForm.ShowDialog();
            }

            if (e.ColumnIndex == 5)
            {
                var authorId = ((AuthorViewModel)(((DataGridView)sender).Rows[e.RowIndex].Cells["AuthorObject"].Value)).Id;

                var bookList = new BookList(authorId);
                bookList.Show();
            }
        }

        private void AuthorForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            FillGrid();
        }
    }
}
